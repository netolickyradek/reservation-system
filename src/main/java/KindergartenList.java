import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KindergartenList {
    private final List<Kindergarten> kindergartens = new ArrayList<>();
    public List<Kindergarten> getKindergartens() {
        return Collections.unmodifiableList(kindergartens);
    }
    public void addKindergarten(Kindergarten kindergarten) {
        kindergartens.add(kindergarten);
    }
    public void removeKindergarten(int index) {
            kindergartens.remove(index);
    }
}
