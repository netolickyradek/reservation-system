import java.time.LocalDate;

public class Kindergarten {

    private String name;
    private String location;
    private int oneOrTwoIntroduction;
    private String telNumber;
    private boolean complete;
    private LocalDate whenCall;
    private LocalDate lastVisit;
    private int oneOrTwoYears;
    private String notes;

    public Kindergarten(String name, String location, int oneOrTwoIntroduction, String telNumber, boolean complete, LocalDate whenCall, LocalDate lastVisit, int oneOrTwoYears, String notes) {
        this.name = name;
        this.location = location;
        this.oneOrTwoIntroduction = oneOrTwoIntroduction;
        this.telNumber = telNumber;
        this.complete = complete;
        this.whenCall = whenCall;
        this.lastVisit = lastVisit;
        this.oneOrTwoYears = oneOrTwoYears;
        this.notes = notes;
    }

    public Kindergarten() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getOneOrTwoIntroduction() {
        return oneOrTwoIntroduction;
    }

    public void setOneOrTwoIntroduction(short oneOrTwoIntroduction) {
        this.oneOrTwoIntroduction = oneOrTwoIntroduction;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LocalDate getWhenCall() {
        return whenCall;
    }

    public void setWhenCall(LocalDate whenCall) {
        this.whenCall = whenCall;
    }

    public LocalDate getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(LocalDate lastVisit) {
        this.lastVisit = lastVisit;
    }

    public int getOneOrTwoYears() {
        return oneOrTwoYears;
    }

    public void setOneOrTwoYears(short oneOrTwoYears) {
        this.oneOrTwoYears = oneOrTwoYears;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
