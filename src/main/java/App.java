import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import javax.swing.*;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class App extends JFrame {

    private KindergartenList kindergartenList = new KindergartenList();
    private final KindergartenListTableModel tableModel = new KindergartenListTableModel(kindergartenList);
    private final JTable table = new JTable(tableModel);
    private final String jsonFileName = "skolky.json";
    private TableRowSorter<KindergartenListTableModel> sorter;
    private boolean saveClicked = false;
    private boolean sorterClicked = false;


    private App() {
        loadData();
        scheduleNotifications();

        setTitle("Rezervační systém školek");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                showCloseConfirmationDialog();
            }
        });

        add(new JScrollPane(table), BorderLayout.CENTER, SwingConstants.CENTER);

        JPanel topPanel = new JPanel();
        add(topPanel, BorderLayout.NORTH);

        JPanel bottomPanel = new JPanel();
        add(bottomPanel, BorderLayout.SOUTH);

        JLabel filterLabel = new JLabel("Filtr lokality: ");
        bottomPanel.add(filterLabel);

        JTextField filterField = new JTextField(20);
        bottomPanel.add(filterField);
        filterField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = filterField.getText();
                if (text.isEmpty()) {
                    sorter.setRowFilter(null);
                } else {
                    sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
        });

        JButton btnAdd = new JButton("Přidat školku");
        topPanel.add(btnAdd, BorderLayout.SOUTH);
        btnAdd.addActionListener(e -> addKindergarten());

        JButton btnRem = new JButton("Odstranit školku");
        topPanel.add(btnRem);
        btnRem.addActionListener(e -> deleteKindergarten());

        JButton btnUpdateDate = new JButton("Aktualizovat datum volání");
        topPanel.add(btnUpdateDate, BorderLayout.SOUTH);
        btnUpdateDate.addActionListener(e -> updateCallDate());

        JButton btnUpdateVisitDate = new JButton("Aktualizovat datum poslední návštěvy");
        topPanel.add(btnUpdateVisitDate, BorderLayout.SOUTH);
        btnUpdateVisitDate.addActionListener(e -> updateVisitDate());

        JButton btnSave = new JButton("Uložit");
        topPanel.add(btnSave);
        btnSave.addActionListener(e -> saveData());

        JButton btnRefresh = new JButton("Obnovit seřazení");
        topPanel.add(btnRefresh);
        btnRefresh.addActionListener(e -> refreshSorter());

        setColumnWidths();

        table.setAutoCreateRowSorter(true);
        sorter = (TableRowSorter<KindergartenListTableModel>) table.getRowSorter();
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        sortKeys.add(new RowSorter.SortKey(0, SortOrder.UNSORTED));

        sorter.setSortKeys(sortKeys);
        table.setRowSorter(sorter);

        pack();
    }

    private void refreshSorter() {
        sorterClicked = true;
        List<RowSorter.SortKey> sortKeys = new ArrayList<>();
        sortKeys.add(new RowSorter.SortKey(0, SortOrder.UNSORTED));

        sorter.setSortKeys(sortKeys);
        sorter.sort();
    }

    private void updateCallDate() {
        if (sorterClicked) {
            int selectedRow = table.getSelectedRow();
            if (selectedRow != -1) {
                String inputDate = JOptionPane.showInputDialog("Zadej nové datum volání (formát: dd.mm.rrrr)");
                if (inputDate != null && !inputDate.isEmpty()) {
                    try {
                        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                        LocalDate newDate = LocalDate.parse(inputDate, dateFormatter);

                        Kindergarten kindergarten = kindergartenList.getKindergartens().get(selectedRow);
                        kindergarten.setWhenCall(newDate);

                        tableModel.fireTableRowsUpdated(selectedRow, selectedRow);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Chyba při zpracování data. Zadejte platné datum.");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nejdříve vyberte řádek pro aktualizaci.");
            }
            sorterClicked = false;
        } else JOptionPane.showMessageDialog(null, "Nejdříve obnov seřazení pomocí tlačítka!");

    }

    private void updateVisitDate() {
        if (sorterClicked) {
            if (table.getSelectedRow() != -1) {
                String inputDate = JOptionPane.showInputDialog("Zadej nové datum poslední návštěvy (formát: dd.mm.rrrr)");
                if (inputDate != null && !inputDate.isEmpty()) {
                    try {
                        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                        LocalDate newDate = LocalDate.parse(inputDate, dateFormatter);

                        int modelRow = table.convertRowIndexToModel(table.getSelectedRow());
                        Kindergarten kindergarten = kindergartenList.getKindergartens().get(modelRow);
                        kindergarten.setLastVisit(newDate);

                        tableModel.fireTableDataChanged();
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "Chyba při zpracování data. Zadejte platné datum.");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nejdříve vyberte řádek pro aktualizaci.");
            }
            sorterClicked = false;
        } else JOptionPane.showMessageDialog(null, "Nejdříve obnov seřazení pomocí tlačítka!");
    }

    private void scheduleNotifications() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        scheduler.scheduleAtFixedRate(() -> {
            LocalDate today = LocalDate.now();

            for (Kindergarten kindergarten : kindergartenList.getKindergartens()) {
                if (kindergarten.getWhenCall().equals(today)) {
                    sendNotification(kindergarten.getName());
                    showKindergartenDetails(kindergarten);
                }
            }
        }, 0, 1, TimeUnit.DAYS);
    }

    private void showKindergartenDetails(Kindergarten kindergarten) {
        JFrame detailsFrame = new JFrame("Detaily školky - " + kindergarten.getName());
        detailsFrame.setSize(400, 300);
        detailsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel detailsPanel = createDetailsPanel(kindergarten);
        detailsFrame.add(detailsPanel);

        detailsFrame.setVisible(true);
    }

    private JPanel createDetailsPanel(Kindergarten kindergarten) {
        JPanel detailsPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(5, 5, 5, 5);

        addDetailLabel(detailsPanel, gbc, "Název:", kindergarten.getName());
        addDetailLabel(detailsPanel, gbc, "Lokalita:", kindergarten.getLocation());
        addDetailLabel(detailsPanel, gbc, "Počet vystoupení:", Integer.toString(kindergarten.getOneOrTwoIntroduction()));
        addDetailLabel(detailsPanel, gbc, "Tel. číslo:", kindergarten.getTelNumber());
        addDetailLabel(detailsPanel, gbc, "Datum volání:", kindergarten.getWhenCall().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        addDetailLabel(detailsPanel, gbc, "Poslední návštěva:", kindergarten.getLastVisit().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        addDetailLabel(detailsPanel, gbc, "Za rok nebo za dva:", Integer.toString(kindergarten.getOneOrTwoYears()));
        addDetailLabel(detailsPanel, gbc, "Poznámky:", kindergarten.getNotes());

        return detailsPanel;
    }

    private void addDetailLabel(JPanel panel, GridBagConstraints gbc, String label, String value) {
        JLabel labelComponent = new JLabel(label);
        gbc.gridx = 0;
        gbc.gridy++;
        panel.add(labelComponent, gbc);

        JLabel valueComponent = new JLabel(value);
        gbc.gridx = 1;
        panel.add(valueComponent, gbc);
    }


    private void sendNotification(String kindergartenName) {
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().createImage("icon.png");

            TrayIcon trayIcon = new TrayIcon(image, "Oznámení");
            trayIcon.setImageAutoSize(true);

            try {
                tray.add(trayIcon);

                LocalDate today = LocalDate.now();
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                String todayString = today.format(dateFormatter);

                trayIcon.displayMessage("!!! Připomínka !!!", "Dnes je den k zavolání do školky " + kindergartenName
                        + "\nDnešní datum: " + todayString, TrayIcon.MessageType.WARNING);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadData() {
        ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
        objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
        try {
            kindergartenList = objectMapper.readValue(new File(jsonFileName), KindergartenList.class);
            tableModel.setKindergartenList(kindergartenList);
            tableModel.fireTableDataChanged();
        } catch (IOException ex) {
            throw new RuntimeException("Problém s načítáním souboru - Kontaktuj Radixe" + ex);
        }
    }

    private void setColumnWidths() {
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(262);
        columnModel.getColumn(1).setPreferredWidth(162);
        columnModel.getColumn(2).setPreferredWidth(41);
        columnModel.getColumn(3).setPreferredWidth(63);
        columnModel.getColumn(4).setPreferredWidth(63);
        columnModel.getColumn(5).setPreferredWidth(68);
        columnModel.getColumn(6).setPreferredWidth(77);
        columnModel.getColumn(7).setPreferredWidth(51);
        columnModel.getColumn(8).setPreferredWidth(746);
    }

    private void showCloseConfirmationDialog() {
        if (!saveClicked) {
            int choice = JOptionPane.showConfirmDialog(this, "Chcete uložit změny?", "Uložení", JOptionPane.YES_NO_CANCEL_OPTION);

            if (choice == JOptionPane.YES_OPTION) {
                saveData();
                setDefaultCloseOperation(EXIT_ON_CLOSE);
            } else if (choice == JOptionPane.NO_OPTION) {
                setDefaultCloseOperation(EXIT_ON_CLOSE);
                dispose();
            }
        } else setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void addKindergarten() {
        String name = JOptionPane.showInputDialog("Zadej název školky");
        String location = JOptionPane.showInputDialog("Zadej lokalitu");
        int oneOrTwoIntro = Integer.parseInt(JOptionPane.showInputDialog("Zadej počet vystoupení"));
        String telNumber = JOptionPane.showInputDialog("Zadej tel. číslo");

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate whenCall = LocalDate.parse(JOptionPane.showInputDialog("Zadej datum kdy volat (formát: dd.mm.rrrr)"), dateFormatter);
        LocalDate lastVisit = LocalDate.parse(JOptionPane.showInputDialog("Datum poslední návštěvy (formát: dd.mm.rrrr)"), dateFormatter);
        int oneOrTwoYears = Integer.parseInt(JOptionPane.showInputDialog("Za rok nebo za dva"));
        String notes = JOptionPane.showInputDialog("Poznámky - cena, začátek vystoupení atd.");

        Kindergarten kindergarten = new Kindergarten(name, location, oneOrTwoIntro, telNumber, false, whenCall, lastVisit, oneOrTwoYears, notes);
        kindergartenList.addKindergarten(kindergarten);

        tableModel.fireTableDataChanged();
    }

    private void deleteKindergarten() {
        int choice = JOptionPane.showConfirmDialog(this, "Opravdu odstranit? Nelze vrátit!", "Odstranění", JOptionPane.YES_NO_CANCEL_OPTION);
        if (choice == JOptionPane.YES_OPTION) {
            if (table.getSelectedRow() != -1) {
                int modelRow = table.convertRowIndexToModel(table.getSelectedRow());
                tableModel.removeRow(modelRow);
                tableModel.fireTableDataChanged();

                sorter = (TableRowSorter<KindergartenListTableModel>) table.getRowSorter();
                sorter.setRowFilter(null);
                sorter.setModel(tableModel);

                JOptionPane.showMessageDialog(null, "Úspěsně smazáno");
            }
        }
    }

    private void saveData() {
        ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
        try {
            objectMapper.writeValue(new File(jsonFileName), kindergartenList);
        } catch (IOException ex) {
            throw new RuntimeException("Chyba s uložením souboru - Kontaktuj Radixe" + ex);
        }
        saveClicked = true;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            App app = new App();
            app.setVisible(true);
            app.setExtendedState(JFrame.MAXIMIZED_BOTH);
        });
    }
}
