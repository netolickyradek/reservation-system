import javax.swing.table.DefaultTableModel;
import java.time.LocalDate;

public class KindergartenListTableModel extends DefaultTableModel {

    private KindergartenList kindergartenList;

    public KindergartenListTableModel(KindergartenList kindergartenList) {
        this.kindergartenList = kindergartenList;
    }


    @Override
    public int getRowCount() {
        if (kindergartenList == null) {
            return 0;
        }
        return kindergartenList.getKindergartens().size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Kindergarten kindergarten = kindergartenList.getKindergartens().get(row);
        switch (column) {
            case 0:
                return kindergarten.getName();
            case 1:
                return kindergarten.getLocation();
            case 2:
                return kindergarten.getOneOrTwoIntroduction();
            case 3:
                return kindergarten.getTelNumber();
            case 4:
                return kindergarten.isComplete();
            case 5:
                return kindergarten.getWhenCall();
            case 6:
                return kindergarten.getLastVisit();
            case 7:
                return kindergarten.getOneOrTwoYears();
            case 8:
                return kindergarten.getNotes();
        }
        throw new RuntimeException("Neznámý sloupec!!" + column);
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Název";
            case 1:
                return "Lokalita";
            case 2:
                return "Počet vystoupení";
            case 3:
                return "Tel.číslo";
            case 4:
                return "Domluveno";
            case 5:
                return "Kdy volat";
            case 6:
                return "Poslední návštěva";
            case 7:
                return "1/2 roky";
            case 8:
                return "Poznámka";
        }
        throw new RuntimeException("Neznámý sloupec!!" + column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 1:
            case 3:
            case 8:
                return String.class;
            case 5:
            case 6:
                return LocalDate.class;
            case 2:
            case 7:
                return Short.class;
            case 4:
                return Boolean.class;
        }
        throw new RuntimeException("Neznámý sloupec!!" + columnIndex);
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        Kindergarten kindergarten = kindergartenList.getKindergartens().get(row);
        switch (column) {
            case 0:
                kindergarten.setName((String) aValue);
                break;
            case 1:
                kindergarten.setLocation((String) aValue);
                break;
            case 2:
                assert aValue instanceof Short;
                kindergarten.setOneOrTwoIntroduction((Short) aValue);
                break;
            case 3:
                assert aValue instanceof String;
                kindergarten.setTelNumber((String) aValue);
                break;
            case 4:
                assert aValue instanceof Boolean;
                kindergarten.setComplete((Boolean) aValue);
                break;
            case 5:
                assert aValue instanceof LocalDate;
                kindergarten.setWhenCall((LocalDate) aValue);
                break;
            case 6:
                kindergarten.setLastVisit((LocalDate) aValue);
                break;
            case 7:
                kindergarten.setOneOrTwoYears((Short) aValue);
                break;
            case 8:
                assert aValue instanceof String;
                kindergarten.setNotes((String) aValue);
                break;
        }
//        fireTableCellUpdated(row, column);
    }

    public void removeRow(int rowIndex) {
        kindergartenList.removeKindergarten(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public void setKindergartenList(KindergartenList kindergartenList) {
        this.kindergartenList = kindergartenList;
        fireTableDataChanged();
    }
}
